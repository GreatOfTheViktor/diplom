from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager

import time


class WebDriver:
    def __init__(self, path_to_driver: str = 'chromedriver.exe') -> None:
        """
        Инициализация вебдрайвера для парсинга html
        :param path_to_driver: Путь к файлу chromedriver
        :return: Инициализированный chromedriver
        """
        self._driver = webdriver.Chrome(ChromeDriverManager().install())


    def get_patent_html(self, patent_link: str) -> str:
        """
        Получить html код страницы
        :param patent_link: Ссылка на патент
        :return: исходный html код страницы
        """
        self._driver.get(patent_link)
        time.sleep(5)
        return self._driver.page_source


    def quit(self) -> None:
        self._driver.quit()