import time
from os import walk

from datetime import datetime

import clickhouse_connect

from patent import Patent
from patent_fields import PatentFields
from patent_loader import PatentLoader
from webdriver import WebDriver
from model import predict_images, convert_images


patents_directory = 'patents/'
patents_images = 'patents_image/'
model_path = 'ParsingYandexPatents/model'


def main():
    
    client = clickhouse_connect.get_client(
        host='se9hjs9cbn.eu-west-1.aws.clickhouse.cloud',
        port=8443,
        username='default',
        password='Ab_7EJk85KMvV'
    )

    client.command('CREATE DATABASE IF NOT EXISTS yandex_patents')
    client.command('''
        CREATE TABLE IF NOT EXISTS yandex_patents.patents_data
        (
            `id` String,
            `title` String,
            `assignee` String,
            `authors` Array(String),
            `publications` Array(Date),
            `link` String,
            `classifications` Array(String),
            `abstract` String,
            `description` String,
            `claims` String,
            `cited` Array(String),
            `class` Array(Int32),
            `formulas` Array(String)
        )
        ENGINE = MergeTree
        PRIMARY KEY (id)'''
    )

    driver = WebDriver('C:\\Work\\aaa\\selenium_parser-master\\chromedriver_win32\\chromedriver.exe')
    loader = PatentLoader(driver)
    parser = PatentFields(driver)

    _, _, year_files = next(walk(patents_directory))

    for year_file in year_files:
        patent_links = loader.get_patent_links(patents_directory + year_file)

        for patent_link in patent_links:
            patent = Patent(patent_link=patent_link, patent_parser=parser)
            folder_name = 'patents_image/' + patent_link.split("/")[-1].split("_")[0]
            client.insert(
                'yandex_patents.patents_data',
                [[
                    patent.get_id(),
                    patent.get_title(),
                    patent.get_assignee(),
                    patent.get_authors(),
                    [datetime.strptime(publication, '%Y-%m-%d') for publication in patent.get_publications()],
                    patent_link,
                    patent.get_classifications(),
                    patent.get_abstract(),
                    patent.get_description(),
                    patent.get_claims(),
                    patent.get_cited(),
                    tuple(predict_images(model_path, folder_name)),
                    convert_images(model_path, folder_name)
                ]],
                column_names=[
                    'id',
                    'title',
                    'assignee',
                    'authors',
                    'publications',
                    'link',
                    'classifications',
                    'abstract',
                    'description',
                    'claims',
                    'cited',
                    'class',
                    'formulas'
                ])

            time.sleep(15)

    client.close()


if __name__ == "__main__":
    main()